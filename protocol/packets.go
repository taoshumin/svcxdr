/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//go:generate -command genxdr go run github.com/calmh/xdr/cmd/genxdr
//go:generate genxdr -o packets_xdr.go packets.go

package protocol

const (
	messageTypePing int32 = iota
	messageTypePong
	messageTypeJoinRelayRequest
	messageTypeJoinSessionRequest
	messageTypeResponse
	messageTypeConnectRequest
	messageTypeSessionInvitation
	messageTypeRelayFull
)

type header struct {
	magic         uint32
	messageType   int32
	messageLength int32
}

type Ping struct{}
type Pong struct{}
type JoinRelayRequest struct{}
type RelayFull struct{}

type JoinSessionRequest struct {
	Key []byte // max:32
}

type Response struct {
	Code    int32
	Message string
}

type ConnectRequest struct {
	ID []byte // max:32
}

type SessionInvitation struct {
	From         []byte // max:32
	Key          []byte // max:32
	Address      []byte // max:32
	Port         uint16
	ServerSocket bool
}
