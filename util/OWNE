func (f *sendReceiveFolder) pullerRoutine(snap *db.Snapshot, in <-chan pullBlockState, out chan<- *sharedPullerState) {
	requestLimiter := util.NewSemaphore(f.PullerMaxPendingKiB * 1024)
	wg := sync.NewWaitGroup()

	for state := range in {
		if state.failed() != nil {
			out <- state.sharedPullerState
			continue
		}

		f.setState(FolderSyncing) // Does nothing if already FolderSyncing

		// The requestLimiter limits how many pending block requests we have
		// ongoing at any given time, based on the size of the blocks
		// themselves.

		state := state
		bytes := int(state.block.Size)

		if err := requestLimiter.TakeWithContext(f.ctx, bytes); err != nil {
			state.fail(err)
			out <- state.sharedPullerState
			continue
		}

		wg.Add(1)

		go func() {
			defer wg.Done()
			defer requestLimiter.Give(bytes)

			f.pullBlock(state, snap, out)
		}()
	}
	wg.Wait()
}
